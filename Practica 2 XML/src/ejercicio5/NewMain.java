/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio5;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author david
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here 
        String comprobacion;
        String afirmativo = "si";

        System.out.println("Bienvenido a nuestra aula \n"
                + "Aqui podemos añadir un nuevo alumno para nuestra clase \n"
                + "Si deseas añadir un alumno tan solo tienes que introducir la palabra: si a continuacion");

        Scanner sc = new Scanner(System.in);

        try {

            comprobacion = sc.nextLine();

            if (comprobacion.equals(afirmativo)) {

                Domicilio dom = new Domicilio();
                Domicilio domi1 = new Domicilio();
                Alumno alumno1 = new Alumno(dom);
                Alumno alumno2 = new Alumno(domi1);

                Aula aula = new Aula(2);
                aula.add(alumno1);
                aula.add(alumno2);

                XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
                //XStream xstream = new XStream(new DomDriver());

                xstream.alias("aula", Aula.class);
                xstream.alias("alumno", Alumno.class);
                xstream.aliasField("fecha", Alumno.class, "anoNacimiento");
                xstream.aliasField("domicilio", Alumno.class, "dom");

                String salida = xstream.toXML(aula);

                System.out.println(salida);

            }else{
                System.err.print("Hemos llegado hasta aqui para que te equivoques??? \n"
                        + "Recuerda, para añadir un nuevo alumno introduce la palabra si y sigue los pasos");
                
            }

        } catch (Exception excpt) {

            System.err.print("ERROR");
        }

    }

}
