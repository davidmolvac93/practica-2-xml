/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio5;

import java.util.Scanner;

/**
 *
 * @author david
 */
public class Domicilio {
    
    private String calle;
    private int domicilio;

    public Domicilio() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Por favor introduce la calle a continuacion");
        calle = sc.next();
        this.calle = calle;
        
        System.out.println("Por favor introduce el numero a continuacion");
        domicilio = sc.nextInt();
        this.domicilio = domicilio;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public int getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(int domicilio) {
        this.domicilio = domicilio;
    }
    
    
}
