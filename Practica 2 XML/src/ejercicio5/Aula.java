/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio5;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author david
 */
public class Aula {

    private List<Alumno> alumnos;
    private Date fechaCreacion;

    /**
     * Constructor del Almacén con un determinado tamano
     *
     * @param tamano
     */
    
    /*
    public Aula(List<Alumno> alumnos, Date fechaCreacion) {
        this.alumnos = alumnos;
        this.fechaCreacion = fechaCreacion;

    }
*/
    public Aula(int tamano) {
        alumnos = new ArrayList<Alumno>(tamano);
    }

    /**
     * Anade un nuevo elemento al almacén si hay sitio
     *
     * @param valor a anadir al almacén
     */
    public void add(Alumno alumno) {
        alumnos.add(alumno);
    }

    /**
     * Elimina un elemento del almacén si está en el almacen
     *
     * @param valor
     * @return true si elimina el elemento, false en caso contrario
     */
    public boolean eliminar(Alumno alumno) {
        return alumnos.remove(alumno);

    }

    public List<Alumno> getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(List<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public String toString() {
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.FULL,
                new Locale("es"));
        return "Aula [alumnos=" + alumnos.toString() + ", fechaCreacion=" + formatter.format(fechaCreacion)
                + "]";
    }
}
