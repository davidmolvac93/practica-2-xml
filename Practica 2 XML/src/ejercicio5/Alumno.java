/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio5;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author david
 */
public class Alumno {

    String nombre;
    String apellidos;
    int anoNacimiento;
    Domicilio dom;
    //private List<Domicilio> casas;

    public Alumno(Domicilio dom) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Por favor introduce el nombre");
        nombre = sc.nextLine();
        this.nombre = nombre;
        
        System.out.println("Por favor introduce el apellido");
        apellidos = sc.nextLine();
        this.apellidos = apellidos;
        
        System.out.println("Por favor introduce el año de nacimiento");
        anoNacimiento = sc.nextInt();
        this.anoNacimiento = anoNacimiento;
        
        this.dom = dom;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getAnoNacimiento() {
        return anoNacimiento;
    }

    public void setAnoNacimiento(int anoNacimiento) {
        this.anoNacimiento = anoNacimiento;
    }

    @Override
    public String toString() {
        return "Alumno [nombre=" + nombre + ", apellidos=" + apellidos
                + ", anoNacimiento=" + anoNacimiento + "]";
    }
}
