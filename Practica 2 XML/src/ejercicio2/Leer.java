/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author david
 */
public class Leer extends DefaultHandler{

    public Leer(){
        super();
    }
    
    public void startDocument(){
        
        System.out.println("-> Empezamos a leer el documento gamers.xml:");
        
    }
    
    public void startElement(String uri, String nombre,String nombreC, Attributes atts){
        
        System.out.println("\t El principo del elemento es: " + nombre);
        
    }
    
    public void endElement(String uri, String nombre, String nombreC){
        
        System.out.println("\t El final del elemento es: " + nombre);
        
    }
    
    public void characters(char [] ch, int inicio, int longitud){
        
        String car = new String(ch, inicio, longitud);
        car = car.replaceAll("[\t\n]","") ; //Quitar salto de linea
        if (!car.isEmpty()) {
            System.out.println("\t " + car);
        }
    }
    
    public void endDocument(){
       
        System.out.println(" -> Cerramos el documento gamers.xml");
    }
    
}
