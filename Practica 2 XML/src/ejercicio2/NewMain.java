/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import java.io.IOException;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author david
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        XMLReader procesadorXML;

        try{
                
            procesadorXML = XMLReaderFactory.createXMLReader();
            Leer gestor = new Leer();
            procesadorXML.setContentHandler((ContentHandler)gestor);
            InputSource fileXML = new InputSource("gamers.xml");
            procesadorXML.parse(fileXML);
            
        }catch(SAXException excpt){
            excpt.printStackTrace();
        }
    }
    
}
