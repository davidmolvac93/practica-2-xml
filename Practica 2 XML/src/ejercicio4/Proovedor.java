/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio4;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author david
 */
public class Proovedor {

    private int identificador;
    private String nombre;
    private String calle;
    private String ciudad;
    private String pais;
    private int cp;
    private boolean esNacional;
    private List<Cafe> cafes;

            //Constructor de proveedor
    public Proovedor(int identificador, String nombre, String calle, String ciudad, String pais, int cp, boolean esNacional, List<Cafe> cafes) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.calle = calle;
        this.ciudad = ciudad;
        this.pais = pais;
        this.cp = cp;
        this.esNacional = esNacional;
        this.cafes = cafes;
    }

    
    public Proovedor() {
        super();
        cafes = new ArrayList<Cafe>(); //pasamos un arraylist de la clase cafe
    }


    public void addCafe(Cafe cafe) {
        cafes.add(cafe);
    }

    public boolean isEsNacional() {
        return esNacional;
    }

    public void setEsNacional(boolean esNacional) {
        this.esNacional = esNacional;
    }

    public List<Cafe> getCafes() {
        return cafes;
    }

    public void setCafes(List<Cafe> cafes) {
        this.cafes = cafes;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    
    @Override
    public String toString() {
        return "Proovedor [identificador=" + identificador + ", nombre="
                + nombre + ", calle=" + calle + ", ciudad=" + ciudad
                + ", pais=" + pais + ", cp=" + cp + ", esNacional="
                + esNacional + ", cafes=" + cafes.toString() + "]";
    }

}
