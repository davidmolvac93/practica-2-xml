/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio4;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.ArrayList;

/**
 *
 * @author david
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

            //creamos arraylist
        ArrayList<Cafe> cafes = new ArrayList<Cafe>();

            //creamos los cafes
        Cafe cafe1 = new Cafe("Arabia", (float) 1.30, 5, 10);
        Cafe cafe2 = new Cafe("Colombia", (float) 1.50, 5, 20);

            //añadimos
        cafes.add(cafe1);
        cafes.add(cafe2);

            //creamos el unico provedor
        Proovedor p1 = new Proovedor(150, "Mi proveedor", "MI CALLE", "madrid", "españita", 28050, true, cafes);

        XStream xstream = new XStream(new DomDriver());
        //String salida = xstream.toXML(cafe1) + "\n" + xstream.toXML(cafe2);

        //System.out.println(salida);
        //XStream xstream = new XStream(new DomDriver());
        //XStream xstream2 = new XStream(new JsonHierarchicalStreamDriver());  //PARA HACER EL JSON   RECUERDA 1 PUNTO MAS
        
        xstream.alias("Provedor", Proovedor.class);
        
            //pasamos los alias y los atributos que deseamos cambiar
        xstream.aliasAttribute(Proovedor.class, "identificador", "150");
        xstream.aliasAttribute(Proovedor.class, "nombre", "Mi proveedor");
        xstream.aliasField("cif", Proovedor.class, "identificador");
        xstream.aliasField("empresa", Proovedor.class, "nombre");
        xstream.alias("cafe", Cafe.class);
        String salida = xstream.toXML(p1);
        System.out.println((salida));
        
        /*
        Cafe cafe3 = (Cafe)xstream.fromXML(salida);
        salida = xstream.toXML(p1);
        System.out.println(cafe3); */
    }

}
