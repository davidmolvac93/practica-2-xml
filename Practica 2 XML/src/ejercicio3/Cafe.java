/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio3;

/**
 *
 * @author david
 */
public class Cafe {
    
    	private String marca;
	private float precio;
	private int ventas;
	private int total;

    public Cafe(String marca, float precio, int ventas, int total) {
        this.marca = marca;
        this.precio = precio;
        this.ventas = ventas;
        this.total = total;
    }
        
        

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	
	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public int getVentas() {
		return ventas;
	}

	public void setVentas(int ventas) {
		this.ventas = ventas;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	 @Override
	    public String toString() {
	        return "Cafe{" + "marca=" + marca + "precio=" + precio+ "ventas=" + ventas + "total=" + total +'}';
	    }
}
