/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio3;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 *
 * @author david
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Cafe cafe1 = new Cafe("Arabia", (float) 1.30, 5, 10);
        Cafe cafe2 = new Cafe("Colombia", (float) 1.50, 5, 20);

        XStream xstream = new XStream(new DomDriver());
        xstream.alias("cafe", Cafe.class);
        String salida = xstream.toXML(cafe1) + "\n" + xstream.toXML(cafe2);
        

        System.out.println(salida);
        //XStream xstream2 = new XStream(new DomDriver());
        //XStream xstream2 = new XStream(new JsonHierarchicalStreamDriver());  //PARA HACER EL JSON   RECUERDA 1 PUNTO MAS
        //String vemos = xstream2.toXML(cafe1) + "\n" + xstream2.toXML(cafe2);
        //System.out.println((vemos));
    }

}
