/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author david
 */
public class Juego {

    String comprobarAñadir = "insertar";
    String comprobarBorrar = "borrar";
    Scanner sc = new Scanner(System.in);
    String palabra = sc.next();

    public void iniciar() {

        if (palabra.equals(comprobarAñadir) || palabra.equalsIgnoreCase(comprobarAñadir)) {
            añadirJuego();
        }

        if (palabra.equals(comprobarBorrar) || palabra.equalsIgnoreCase(comprobarBorrar)) {
            borrarJuego();
        }

    }

    public void añadirJuego() {

            //Metodo para añadir un juego
        System.out.println("Te gusta gastar billetes eh, venga dime que juego nuevo quieres añadir ");

        try {
                
                //Comenzamos con la creacion de documento
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            Scanner sc = new Scanner(System.in);
            String fichero = "juegos.xml";

            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new File(fichero));
            document.getDocumentElement().normalize(); //necesario el normalize

            //Creamos una lista con todos los nodos de los juegos, estilo al ejej de practica que vimos de comida
            //NodeList juegos = document.getElementsByTagName("juego");
            Element raiz = document.createElement("juego");
            document.getDocumentElement().appendChild(raiz);

            String titulo, genero, plataforma, fechadelanzamiento;

            System.out.println("Inserta el titulo del juego por favor");
            titulo = sc.nextLine();
            System.out.println("Insertar el genero del videojuego");
            genero = sc.nextLine();
            System.out.println("Inserta a que plataforma pertenece el juego");
            plataforma = sc.nextLine();
            System.out.println("Inserta la fecha de lanzamiento del juego");
            fechadelanzamiento = sc.nextLine();

            //nombre del videojuego
            if (titulo.isEmpty()) {
                System.out.println("Error introduce un nombre, no puede quedarse en blanco");

            } else {
                    //creamos titulo
                Element elm = document.createElement("titulo");
                Text text = document.createTextNode(titulo);
                raiz.appendChild(elm);
                elm.appendChild(text);
            }

            //su genero
            if (genero.isEmpty()) {
                System.out.println("Error introduce un genero, no puede quedarse en blanco");
            } else {
                    //creamos genero
                Element elmr = document.createElement("genero");
                Text text5 = document.createTextNode(genero);
                raiz.appendChild(elmr);
                elmr.appendChild(text5);
            }

            //plataforma a la que pertenece
            if (plataforma.isEmpty()) {
                System.out.println("Error introuce una plataforma, no puede quedarse en blanco");
            } else {
                    //creamos plataforma
                Element elm2 = document.createElement("plataforma");
                Text text2 = document.createTextNode(plataforma);
                raiz.appendChild(elm2);
                elm2.appendChild(text2);
            }

            //fecha de lanzamiento
            if (fechadelanzamiento.isEmpty()) {
                System.out.println("Error introduce un año en el que se lanzo el videojuego");
            } else {
                    //creamos fecha
                Element elml = document.createElement("fechadelanzamiento");
                Text textf = document.createTextNode(fechadelanzamiento);
                raiz.appendChild(elml);
                elml.appendChild(textf);
            }

            //generar un NUEVO con el nombre que ya teniamos
            System.out.println("Juego añadido, revisa tu archivo");

            Source source = new DOMSource(document);
            Result result = new StreamResult(new java.io.File("juegos.xml"));
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, result);
            sc.close();

        } catch (Exception ex) {
            System.err.print("ERROR");
        }
    }

    public void borrarJuego() {

            //Metodo para borrar un juego
        System.out.println("Oh que pena vamos a borrar un juego, esperemos que te lo hayas pasado ya");

        try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            Scanner sc = new Scanner(System.in);
            String fichero = "juegos.xml";      //Al ser uno ya creado debemos pasar XML ya creado

            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new File(fichero));
            document.getDocumentElement().normalize();

            //ELIMINAR UN JUEGO
            System.out.println("Inserta el nombre del juego que quieras borrar ");
            String borrar = sc.nextLine();

            NodeList juegoBorrar = document.getElementsByTagName("juego");
            for (int i = 0; i < juegoBorrar.getLength(); i++) {
                Node juego = juegoBorrar.item(i); //obtener un nodo 
                if (juego.getNodeType() == Node.ELEMENT_NODE) {//tipo de nodo 
                    Element elemento = (Element) juego; //obtener los elementos del nodo

                    NodeList nodo = elemento.getElementsByTagName("titulo").item(0).getChildNodes();
                    Node valornodo = (Node) nodo.item(0);
                    if (valornodo.getNodeValue().equals(borrar)) {
                        juego.getParentNode().removeChild(juego);
                        System.out.println("De acuerdo hemos borrado el juego ");
                    }
                } else {
                    System.out.println("No he podido encontrar un juego con ese nombre dentro de tu lista");
                }
            }

            Source source2 = new DOMSource(document);
            Result result2 = new StreamResult(new java.io.File("juegos.xml"));
            Transformer transformer2 = TransformerFactory.newInstance().newTransformer();
            transformer2.transform(source2, result2);
            sc.close();

        } catch (Exception ex) {

            System.err.print("ERROR A LA HORA DE EJECUTAR");
        }
    }
}
